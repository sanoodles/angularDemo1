import { AngularDemo1Page } from './app.po';

describe('angular-demo1 App', () => {
  let page: AngularDemo1Page;

  beforeEach(() => {
    page = new AngularDemo1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
