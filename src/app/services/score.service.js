"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var ScoreService = (function () {
    function ScoreService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.scoresUrl = 'https://angularjs-demo-1.firebaseio.com/scores/highscores.json'; // URL to web api
    }
    ScoreService.prototype.getScores = function () {
        return this.http.get(this.scoresUrl)
            .toPromise()
            .then(function (response) { return Object.values(response.json()); })
            .catch(this.handleError);
    };
    ScoreService.prototype.save = function (score) {
        return this.http
            .post(this.scoresUrl, JSON.stringify(score), { headers: this.headers })
            .toPromise()
            .catch(this.handleError);
    };
    ScoreService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return ScoreService;
}());
ScoreService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ScoreService);
exports.ScoreService = ScoreService;
//# sourceMappingURL=score.service.js.map