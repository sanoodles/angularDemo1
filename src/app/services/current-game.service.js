"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_state_1 = require("../models/game-state");
var sorted_words_service_1 = require("./sorted-words.service");
var CurrentGameService = (function () {
    function CurrentGameService(sortedWordsService) {
        this.sortedWordsService = sortedWordsService;
        this.gameDurationInSeconds = 40;
        this.onTimeIsUp = function () { };
        this.state = new game_state_1.GameState();
    }
    CurrentGameService.prototype.reset = function () {
        var _this = this;
        this.state.name = '';
        return this.sortedWordsService.getWords()
            .then(function (sortedWords) {
            _this.shuffleArray(sortedWords);
            _this.state.sortedWords = sortedWords;
            _this.state.sortedWordsIterator = 0;
            _this.state.unsortedWord = _this.getUnsortedWord(_this.getSolution());
            _this.state.enteredWord = '';
            _this.state.currentReward = _this.getRewardForWord(_this.getSolution());
            _this.state.score = 0;
            _this.state.timeLeft = _this.gameDurationInSeconds;
        });
    };
    CurrentGameService.prototype.start = function () {
        var _this = this;
        this.timer = setInterval(function () {
            _this.state.timeLeft--;
            if (_this.state.timeLeft < 1) {
                clearInterval(_this.timer);
                _this.onTimeIsUp();
            }
        }, 1000);
    };
    CurrentGameService.prototype.nextWord = function () {
        this.state.sortedWordsIterator++;
        this.state.unsortedWord = this.getUnsortedWord(this.getSolution());
        this.state.enteredWord = '';
        this.state.currentReward = this.getRewardForWord(this.getSolution());
    };
    CurrentGameService.prototype.getSolution = function () {
        return this.state.sortedWords[this.state.sortedWordsIterator];
    };
    CurrentGameService.prototype.end = function () {
        clearInterval(this.timer);
    };
    CurrentGameService.prototype.shuffleArray = function (a) {
        var j, x, i, n = a.length;
        for (i = n; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    };
    CurrentGameService.prototype.getUnsortedWord = function (word) {
        var a = word.split("");
        this.shuffleArray(a);
        return a.join("");
    };
    CurrentGameService.prototype.getRewardForWord = function (word) {
        return Math.floor(Math.pow(1.95, word.length / 3));
    };
    return CurrentGameService;
}());
CurrentGameService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [sorted_words_service_1.SortedWordsService])
], CurrentGameService);
exports.CurrentGameService = CurrentGameService;
//# sourceMappingURL=current-game.service.js.map