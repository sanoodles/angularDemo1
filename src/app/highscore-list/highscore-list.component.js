"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var score_service_1 = require("../services/score.service");
var HighscoreListComponent = (function () {
    function HighscoreListComponent(router, scoreService) {
        this.router = router;
        this.scoreService = scoreService;
        this.highscores = [];
    }
    HighscoreListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.scoreService.getScores()
            .then(function (scores) { return _this.highscores = scores.sort(_this.orderByScoreDesc).slice(0, 10); });
    };
    HighscoreListComponent.prototype.newGame = function () {
        this.router.navigateByUrl('/new-game');
    };
    HighscoreListComponent.prototype.orderByScoreDesc = function (a, b) {
        return b.score - a.score;
    };
    return HighscoreListComponent;
}());
HighscoreListComponent = __decorate([
    core_1.Component({
        selector: 'highscore-list',
        templateUrl: './highscore-list.component.html',
    }),
    __metadata("design:paramtypes", [router_1.Router,
        score_service_1.ScoreService])
], HighscoreListComponent);
exports.HighscoreListComponent = HighscoreListComponent;
//# sourceMappingURL=highscore-list.component.js.map