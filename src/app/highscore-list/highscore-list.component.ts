import { Component, OnInit }  from '@angular/core';

import { Router }         from '@angular/router';

import { Score }          from '../models/score';
import { ScoreService }   from '../services/score.service';

@Component({
  selector: 'highscore-list',
  templateUrl: './highscore-list.component.html',
})
export class HighscoreListComponent  {
  highscores: Score[] = [];

  constructor(
    private router: Router,
    private scoreService: ScoreService,
  ) { }

  ngOnInit(): void {
    this.scoreService.getScores()
        .then(scores => this.highscores = scores.sort(this.orderByScoreDesc).slice(0, 10));
  }

  newGame(): void {
    this.router.navigateByUrl('/new-game');
  }

  private orderByScoreDesc(a: Score, b: Score): number {
    return b.score - a.score;
  }
}

