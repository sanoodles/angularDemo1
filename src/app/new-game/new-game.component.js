"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var current_game_service_1 = require("../services/current-game.service");
var NewGameComponent = (function () {
    function NewGameComponent(router, currentGameService) {
        this.router = router;
        this.currentGameService = currentGameService;
        this.isResetDone = false;
    }
    NewGameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.state = this.currentGameService.state;
        this.currentGameService.reset()
            .then(function () { return _this.isResetDone = true; });
    };
    NewGameComponent.prototype.startGame = function () {
        this.router.navigateByUrl('/game');
        this.currentGameService.start();
    };
    NewGameComponent.prototype.onKeyPress = function (event) {
        if (event.keyCode == 13 &&
            this.isResetDone &&
            this.state.name.length > 0) {
            this.startGame();
        }
    };
    return NewGameComponent;
}());
NewGameComponent = __decorate([
    core_1.Component({
        selector: 'new-game',
        templateUrl: './new-game.component.html',
    }),
    __metadata("design:paramtypes", [router_1.Router,
        current_game_service_1.CurrentGameService])
], NewGameComponent);
exports.NewGameComponent = NewGameComponent;
//# sourceMappingURL=new-game.component.js.map