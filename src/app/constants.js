"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.keyCodes = {
    backspace: 8,
    enter: 13,
    delete: 46,
};
exports.colors = {
    success: '#8f8',
    failure: '#f88',
};
exports.notificationPeriodInMs = 150;
//# sourceMappingURL=constants.js.map