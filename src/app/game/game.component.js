"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var constants_1 = require("../constants");
var current_game_service_1 = require("../services/current-game.service");
var score_service_1 = require("../services/score.service");
var GameComponent = (function () {
    function GameComponent(router, currentGameService, scoreService) {
        this.router = router;
        this.currentGameService = currentGameService;
        this.scoreService = scoreService;
    }
    GameComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.state = this.currentGameService.state;
        this.wordBg = 'transparent';
        this.currentGameService.onTimeIsUp =
            function () { return _this.scoreService.save({ name: _this.state.name, score: _this.state.score }).then(function () {
                window.alert('Time is up. Your score was ' + _this.state.score + '.');
                _this.router.navigateByUrl('/highscore-list');
            }).catch(function () {
                window.alert('Time is up. Your score was ' + _this.state.score + ' but could not be saved.');
                _this.router.navigateByUrl('/highscore-list');
            }); };
    };
    GameComponent.prototype.onKeyDown = function (event) {
        var _this = this;
        if (event.keyCode == constants_1.keyCodes.backspace ||
            event.keyCode == constants_1.keyCodes.delete) {
            if (this.state.enteredWord.length > 0) {
                this.state.currentReward--;
                if (this.state.currentReward < 1) {
                    this.currentGameService.nextWord();
                    this.wordBg = constants_1.colors.failure;
                    setTimeout(function () { return _this.wordBg = 'transparent'; }, constants_1.notificationPeriodInMs);
                }
            }
        }
    };
    GameComponent.prototype.onKeyUp = function (event) {
        var _this = this;
        if (this.state.enteredWord.toLowerCase() == this.currentGameService.getSolution()) {
            this.state.score += this.state.currentReward;
            this.currentGameService.nextWord();
            this.wordBg = constants_1.colors.success;
            setTimeout(function () { return _this.wordBg = 'transparent'; }, constants_1.notificationPeriodInMs);
        }
    };
    GameComponent.prototype.quitGame = function () {
        if (window.confirm('Do you really want to quit the game?')) {
            this.currentGameService.end();
            this.router.navigateByUrl('/highscore-list');
        }
        else {
            this.enteredWordElement.nativeElement.focus();
        }
    };
    return GameComponent;
}());
__decorate([
    core_1.ViewChild('enteredWord'),
    __metadata("design:type", core_1.ElementRef)
], GameComponent.prototype, "enteredWordElement", void 0);
GameComponent = __decorate([
    core_1.Component({
        selector: 'game',
        templateUrl: './game.component.html',
    }),
    __metadata("design:paramtypes", [router_1.Router,
        current_game_service_1.CurrentGameService,
        score_service_1.ScoreService])
], GameComponent);
exports.GameComponent = GameComponent;
//# sourceMappingURL=game.component.js.map