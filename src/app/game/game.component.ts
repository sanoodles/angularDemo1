import {
  Component,
  ViewChild,
  ElementRef
}                     from '@angular/core';
import { Router }     from '@angular/router';

import {
  keyCodes,
  colors,
  notificationPeriodInMs
}                             from '../constants';
import { GameState }          from '../models/game-state';
import { CurrentGameService } from '../services/current-game.service'
import { ScoreService }       from '../services/score.service'

@Component({
  selector: 'game',
  templateUrl: './game.component.html',
})
export class GameComponent {
  @ViewChild('enteredWord') enteredWordElement: ElementRef;

  state: GameState;
  wordBg: string;
  placeholder: string;

  constructor(
    private router: Router,
    private currentGameService: CurrentGameService,
    private scoreService: ScoreService,
  ) { }

  ngOnInit(): void {
    this.state = this.currentGameService.state;
    this.wordBg = 'transparent';
    this.placeholder = this.currentGameService.getSolution();

    this.currentGameService.onTimeIsUp =
        () => this.scoreService.save({ name: this.state.name, score: this.state.score }).then(() => {
          window.alert('Time is up. Your score was ' + this.state.score + '.');
          this.router.navigateByUrl('/highscore-list');
        }).catch(() => {
          window.alert(`Time is up. Your score was ${this.state.score}.

Backend score storage is disabled as this is an only-frontend demo with public access.

Thank you for your understanding.`);
          this.router.navigateByUrl('/highscore-list');
        });
  }

  onKeyDown(event: any): void {
    if (event.keyCode == keyCodes.backspace ||
        event.keyCode == keyCodes.delete) {
      if (this.state.enteredWord.length > 0) {
        this.state.currentReward--;
        if (this.state.currentReward < 1) {
          this.currentGameService.nextWord();
          this.wordBg = colors.failure;
          setTimeout(() => this.wordBg = 'transparent', notificationPeriodInMs);
        }
      }
    }
  }

  onKeyUp(): void {
    if (this.state.enteredWord.toLowerCase() == this.currentGameService.getSolution()) {
      this.state.score += this.state.currentReward;
      this.currentGameService.nextWord();
      this.wordBg = colors.success;
      this.placeholder = '';
      setTimeout(() => this.wordBg = 'transparent', notificationPeriodInMs);
    }
  }

  skipWord(): void {
    this.currentGameService.nextWord();
    this.enteredWordElement.nativeElement.focus();
  }

  quitGame(): void {
    if (window.confirm('Do you really want to quit the game?')) {
      this.currentGameService.end();
      this.router.navigateByUrl('/highscore-list');
    } else {
      this.enteredWordElement.nativeElement.focus();
    }
  }

}

